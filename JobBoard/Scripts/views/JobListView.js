﻿var JobListView = Backbone.View.extend({

    initialize: function (options) {
        this.template = options.template || getTemplate('#jobListTemplate');
        this.itemTemplate = options.itemTemplate || getTemplate('#jobListItemTemplate');

        this.listenTo(this.collection, 'remove', this.render);
    },

    events: {
        'click .edit-action': 'jobEditClick',
        'click .delete-action': 'jobDeleteClick'
    },

    render: function () {
        var self = this,
            jobs = this.collection.map(function (j) {
                return j.attributes;
            });

        self.$el.html('');

        self.$el.html(self.template({
            jobs: jobs,
            itemTemplate: self.itemTemplate
        }));

        return self;
    },

    // event handlers
    jobEditClick: function (e) {
        e.preventDefault();

        var self = this,
            action = $(e.currentTarget),
            jobId = action.data('job-id');

        self.trigger('job:edit', jobId);
    },

    jobDeleteClick: function (e) {
        e.preventDefault();
        if (!confirm('Deleting this job is permanent. You sure you want to do this?')) {
            return false;
        }

        var self = this,
            action = $(e.currentTarget),
            jobId = $(action).data('job-id');

        self.trigger('job:delete', jobId);
    }

});