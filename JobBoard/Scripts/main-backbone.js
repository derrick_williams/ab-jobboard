﻿(function ($) {

    var app = {
        start: function () {
            var self = this, loader = this.loadJobs();
            loader.success(self.renderApp);
            loader.error(self.showAppError);
        },

        loadJobs: function () {
            return new JobCollection().fetch();
        },

        renderApp: function (jobs) {

            var appViewModel = new App({
                headerText: 'MyJobBoard',
                jobList: new JobCollection(jobs)
            }),
            appView = new AppView({
                el: $('.page-wrapper'),
                model: appViewModel
            });

            appView.render();
        },

        renderAppError: function () {
            console.log('FAILED TO LOAD JOB');
        }
    };


    $(function () {
        app.start();

        //var jobs = new JobCollection(),
        //    messenger, app, appView;

        //messenger = _.extend({}, Backbone.Events, {
        //    subscribers: [],

        //    subscribe: function (msg, handler) {
        //        var messageAllowed = _.contains(['job:new', 'job:edit']);
        //        if (!messageIsAllowed) {
        //            console.log('Message [' + msg + '] is not allowed');
        //            return;
        //        }

        //        if (!this.subscribers[msg]) {
        //            this.subscribers[msg] = [];
        //        }

        //        this.subscribers[msg].push(handler);
        //    },

        //    publish: function() {

        //    }
        //});

        //Backbone.View.prototype.appMessenger = messenger

        //jobs.fetch()
        //    .success(function (collection) {
        //        renderApp(jobs);
        //    })
        //    .error(function (collection, response, options) {
        //        showError();
        //    });

        //function renderApp(jobs) {

        //}

        //function showError() {
        //    alert('job load error');
        //}
    });

})(jQuery);