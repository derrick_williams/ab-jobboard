﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using JobBoard.Models.Interfaces;

namespace JobBoard.Models.Repositories
{
    public class JobJsonRepository : IJobRepository
    {
        private string JobsDataFile = HttpContext.Current.Server.MapPath("~/App_Data/jobs.json");

        IEnumerable<Job> JobData()
        {
            return Json.Decode<List<Job>>(File.ReadAllText(JobsDataFile));
        }

        public Job Find(string id)
        {
            var targetJob = JobData().FirstOrDefault(j => j.Id == id);
            if (targetJob == null) throw new Exception("Job not found with ID: " + id);

            return targetJob;
        }

        public IEnumerable<Job> FindAll()
        {
            return JobData();
        }

        public void Store(Job j)
        {
            var jobs = FindAll().ToList();

            if (String.IsNullOrEmpty(j.Id))
            {
                j.Id = System.Guid.NewGuid().ToString();
            }
            else
            {
                var existingJob = Find(j.Id);
                // filter out existing job from dataset
                jobs = RemoveJobById(jobs, existingJob.Id);

                // update existing job details
                existingJob.Title = j.Title;
                existingJob.Description = j.Description;
                existingJob.Company = j.Company;

                // set storing job for addition to dataset
                j = existingJob;
            }
           
            jobs.Add(j);

            SaveData(jobs);
        }
        
        public void Remove(string id)
        {
            var jobs = FindAll().ToList();
            jobs = RemoveJobById(jobs, id);
            SaveData(jobs);
        }

        private List<Job> RemoveJobById(List<Job> jobs, string id)
        {
            return jobs.Where(x => !x.Id.Equals(id)).ToList();
        }

        private void SaveData(List<Job> jobs)
        {
            File.WriteAllText(JobsDataFile, Json.Encode(jobs));
        }
    }
}