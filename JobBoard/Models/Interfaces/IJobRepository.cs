﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobBoard.Models.Interfaces
{
    public interface IJobRepository
    {
         Job Find(string id);
         IEnumerable<Job> FindAll();

         void Store(Job job);
         void Remove(string id);
    }
}
