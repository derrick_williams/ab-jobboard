﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobBoard.Models.Interfaces
{
    public interface ICompanyRepository
    {
        Company Find(string id);
        IEnumerable<Company> FindAll();

        void Store(Company company);
    }
}
