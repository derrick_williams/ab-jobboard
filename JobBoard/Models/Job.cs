﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobBoard.Models
{
    public class Job
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Company { get; set; }
    }

}
