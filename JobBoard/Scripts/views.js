﻿var getTemplate = function (selector) {
    return _.template($(selector).html());
};

// Will handle the main application layout
// assembles all the individual page components
// handles the higher level page events
var AppView = Backbone.View.extend({
    initialize: function (options) {
       
    },

    jobsCollection: function() {
        return this.model.get('jobList');
    },
 
    render: function () {
        var self, appTemplate = getTemplate('#appTemplate'),
            headerTemplate = getTemplate('#jobActionsPanelTemplate'),
            header, jobList;

        this.$el.append(appTemplate());
        header = this.renderHeader(headerTemplate);
        jobList = this.renderJobList();

        this.listenTo(header, 'job:new', this.handleJobNew);
        this.listenTo(jobList, 'job:edit', this.handleJobEdit);
        this.listenTo(jobList, 'job:delete', this.handleJobDelete);
    },

    handleJobNew: function (e) {
        alert('newing!');
    },
    handleJobEdit: function (jobId) {
        alert('editing! ' + e);
    },
    handleJobDelete: function (jobId) {
        this.jobsCollection().deleteJob(jobId);
    },

    renderHeader: function (template) {
        var appHeaderView = new AppHeaderView({
            el: this.$el.find('.app-header-container'),
            template: template,
            headerText: this.model.get('headerText')
        });

        return appHeaderView.render();
    },

    renderJobList: function () {
        var jobListView = new JobListView({
                el: this.$el.find('.job-list-container'),
                collection: this.model.get('jobList')
            });

        return jobListView.render();
    }
});




// Represents the main header and action buttons
var AppHeaderView = Backbone.View.extend({
    initialize: function (options) {
        this.template = options.template;
        this.headerText = options.headerText
    },

    events: {
        'click .job-actions .new-job': 'newJobClick' 
    },

    render: function () {
        this.$el.html(this.template({ headerText: this.headerText }));
        return this;
    },

    // event handlers
    newJobClick: function (e) {
        e.preventDefault();
        this.trigger('job:new');
    }
});





var ModalJobFormView = Backbone.View.extend({
    initialize: function () {
        this.$el.html('');
        this.template = options.template;
        render();
    },
    
    render: function() {
        alert('showing modal view');
    },
    events: {

    },
});