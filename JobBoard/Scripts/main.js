﻿
(function () {
    var jobs = [];

    var renderMainPage = function () {
        var jobActionsTemplate = _.template($('#jobActionsPanelTemplate').html());
        $('.page-wrapper').prepend(jobActionsTemplate());

        $('.job-actions').on('click', function (e) {
            e.preventDefault();
            showJobForm({ Id: '', Title: '', Description: '', Company: '' }, handleJobFormAction);
        });
    };

    var renderJobList = function (err, jobs) {
        if (err) throw err.message;

        var jobListTemplate = _.template($('#jobListTemplate').html());
        var jobItemTemplate = _.template($('#jobListItemTemplate').html());

        $('.main-content').html(jobListTemplate({
            jobs: jobs,
            jobItemTemplate: jobItemTemplate
        }));

        $('.job-list .job-list-item')
            .on('click', 'a.edit-action', function (e) {
                e.preventDefault();
                var listItem = $(this);
                var jobId = listItem.data('job-id');
                var targetJob = _(jobs).find(function (job) {
                    return job.Id == jobId;
                });

                showJobForm(targetJob, handleJobFormAction);
            })
            .on('click', 'a.delete-action', function (e) {
                e.preventDefault();

                var confirmMessage = 'Deleting this job is permanent. You sure you want to do this?';
                if (confirm(confirmMessage)) {
                    var jobId = $(this).data('job-id'),
                    targetJob = _(jobs).find(function (job) {
                        return job.Id == jobId
                    });

                    deleteJob(targetJob, updateJobList);
                }
            });
    };

    var updateJobList = function (err, data) {
        if (err) throw err.message;
        fetchJobs(renderJobList);
    };

    var showJobForm = function (job, callback) {
        var form = $('#jobFormTemplate').html();
        form = _.template(form, job);

        var modal = $('.modal');
        modal.find('.modal-body-content').html(form);
        modal.modal();

        modal
            .off('submit')
            .on('submit', function (e) {
                e.preventDefault();
                var formData = $(e.target).serializeArray(),
                    jobFormValid = validateJobForm(formData);

                if (jobFormValid) {
                    saveJobData(formData, updateJobList);
                    modal.modal('hide');
                }
                else {
                    showInvalidFormMessage(modal.find('.form-message'));
                }
            });
    }

    var handleJobFormAction = function (err, data) {
        if (err) { throw 'Oh no!'; }

        // update job

        // re-render list
    }

    var validateJobForm = function (formData) {
        var titleField = _(formData).findWhere({ name: 'Title' });
        return titleField.value != undefined && titleField.value != '';
    }

    var showInvalidFormMessage = function (messageContainer) {
        var msg = $('<div />').addClass('alert alert-error').text('You have some incorrect data in your job information.');
        messageContainer.html(msg);
    }

    var saveJobData = function (data, callback) {
        var job = {};
        _(data).each(function (field) {
            job[field.name] = field.value;
        });
        $.ajax({
            url: '/api/jobs',
            type: (job.Id == undefined || job.Id == '') ? 'POST' : 'PUT',
            data: job,
            dataType: 'json',
            success: function (res) {
                callback(null, res);
            },
            error: function (xhr, status, msg) {
                console.log('post error');
            }
        });
    };

    var fetchJobs = function (callback) {
        $.ajax({
            url: '/api/jobs',
            dataType: 'json',
            success: function (response) {
                callback(null, response);
            },
            error: function () {
                callback(new Error('Job fetch failed'));
            }
        });
    };
    
    var deleteJob = function (job, callback) {
        $.ajax({
            url: '/api/jobs/' + job.Id,
            type: 'DELETE', 
            dataType: 'text',
            success: function (res) {
                callback(null);
            },
            error: function () {
                callback(new Error('Error deleting job'));
            }
        });
    }

    $(function () {
        renderMainPage();
        fetchJobs(renderJobList);
    });

}());