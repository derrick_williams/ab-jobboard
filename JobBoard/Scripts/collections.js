﻿var JobCollection = Backbone.Collection.extend({
    url: '/api/jobs',
    model: Job,
    deleteJob: function (id) {
        this.findWhere({
            Id: id
        }).destroy();
    }
});