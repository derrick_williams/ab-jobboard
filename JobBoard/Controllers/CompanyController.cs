﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JobBoard.Models;
using JobBoard.Models.Interfaces;
using JobBoard.Models.Repositories;

namespace JobBoard.Controllers
{
    public class CompanyController : ApiController
    {
        private ICompanyRepository repo = new CompanyJsonRepository();

        public IEnumerable<Company> Get()
        {
            return repo.FindAll();
        }

        public Company Get(string id)
        {
            return repo.Find(id);
        }

        public Company Post(Company c)
        {
            repo.Store(c);
            return c;
        }

        public Company Put(Company c)
        {
            repo.Store(c);
            return c;
        }
    }
}
