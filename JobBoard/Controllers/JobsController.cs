﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using JobBoard.Models;
using JobBoard.Models.Interfaces;
using JobBoard.Models.Repositories;

namespace JobBoard.Controllers
{
    public class JobsController : ApiController
    {
        private IJobRepository repo = new JobJsonRepository();

        public IEnumerable<Job> Get()
        {
            return repo.FindAll();
        }

        public Job Get(string id)
        {
            return repo.Find(id);
        }

        public Job Post(Job j)
        {
            repo.Store(j);
            return j;
        }

        public Job Put(Job j)
        {
            repo.Store(j);
            return j;
        }

        public HttpResponseMessage Delete(string id)
        {
            repo.Remove(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
