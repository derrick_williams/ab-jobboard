﻿var App = Backbone.Model.extend({
    initialize: function () {
        this.set('jobCount', this.get('jobList').length);
    },
    defaults: {
        headerText: '[need some text]',
        jobList: []
    }
});

var Job = Backbone.Model.extend({
    baseUlr: '/api/job',
    initialize: function () {

    },
    defaults: {
        Title: "",
        Description: ""
    }
});