﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Helpers;

using JobBoard.Models.Repositories;
using JobBoard.Models;

namespace JobBoard.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Company()
        {
            var repo = new CompanyJsonRepository();
            var companies = new List<Company>
            {
                new Company { Id = generateId(), Name = "Careerbuilder" },
                new Company { Id = generateId(), Name = "McDonald's" }
            };

            companies.ForEach(repo.Store);
            return View();
        }

        private string generateId() 
        {
            return System.Guid.NewGuid().ToString();
        }
    }
}
