﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using JobBoard.Models.Interfaces;

namespace JobBoard.Models.Repositories
{
    public class CompanyJsonRepository : ICompanyRepository
    {
        private string CompaniesDataFile = HttpContext.Current.Server.MapPath("~/App_Data/companies.json");

        IEnumerable<Company> CompanyData()
        {
            return Json.Decode<List<Company>>(File.ReadAllText(CompaniesDataFile));
        }

        public Company Find(string id)
        {
            var targetCompany = CompanyData().FirstOrDefault(c => c.Id == id);
            if (targetCompany == null) throw new Exception("Company not found with ID: " + id);

            return targetCompany;
        }

        public IEnumerable<Company> FindAll()
        {
            return CompanyData();
        }

        public void Store(Company c)
        {
            var companies = FindAll().ToList();

            if (String.IsNullOrEmpty(c.Id))
            {
                c.Id = System.Guid.NewGuid().ToString();
            }
            else
            {
                var existingCompany = Find(c.Id);
                // filter out existing Company from dataset
                companies = companies.Where(x => !x.Id.Equals(c.Id)).ToList();

                // update existing Company details
                existingCompany.Name = c.Name;

                // set storing Company for addition to dataset
                c = existingCompany;
            }

            companies.Add(c);

            var CompanyData = Json.Encode(companies);
            File.WriteAllText(CompaniesDataFile, CompanyData);
        }
    }
}