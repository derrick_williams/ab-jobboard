﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JobBoard.Models
{
    public class Company
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
